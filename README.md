# Maszyny stanów na podstawie SMACH

## Wstęp do ćwiczenia

SMACH to architektura zadaniowa do szybkiego tworzenia złożonych zachowań robotów. W swej istocie SMACH jest niezależną od ROS biblioteką Pythona do tworzenia
hierarchicznych maszyn stanu. SMACH to biblioteka, która wykorzystuje bardzo stare koncepcje, aby szybko tworzyć niezawodne zachowanie robota z 
prostym w obsłudze i modułowym kodem.

* Kiedy można używać SMACH?

SMACH jest przydatny, gdy chcesz, aby robot wykonał skomplikowany plan, w którym wszystkie możliwe stany i przejścia między stanami można wyraźnie opisać.
To zasadniczo ułatwia cały proces programowania, aby systemy takie jak mobilne roboty-manipulatory robiły interesujące rzeczy. Przykładowo:

- Szybkie prototypowanie: prosta składnia SMACH oparta na języku Python ułatwia szybkie prototypowanie maszyny stanów i uruchamianie jej.

- Złożone maszyny stanów: SMACH umożliwia projektowanie, modyfikowanie i debugowanie dużych, złożonych hierarchicznych maszyn stanowych.

- Wgląd w napisany projekt: SMACH umożliwia pełny wgląd w maszyny stanów, przejścia między stanami, przepływ danych. Graficzna wizualizacja maszyny stanów
jest możliwa dzięki programowi Smach Viewer.

* Kiedy nie stosować SMACH?

- Zadania nieustrukturyzowane: SMACH przestanie działać, jeśli organizacja zadań przestanie być ustrukturyzowana.

- Systemy niskiego poziomu: SMACH nie jest przeznaczony do stosowania jako maszyna stanu dla systemów niskiego poziomu, które wymagają wysokiej wydajności.


Z pomocą SMACH można zbudować kompletną maszynę stanów. Umożliwia również wykonywanie oraz koordynację z poziomu zadań (jako biblioteka), udostępnia kilka 
typów "kontenerów" stanu. Taki kontener jest kompletną maszyną stanów sam w sobie oraz może być użyty jako jeden ze stanów większej maszyny. Przykładowo:

- Maszyna Stanów (StateMachine container)

- Sekwencje (Sequence container)

- Pętle/iteracje (Iterator container)

### Instalowanie potrzebnych pakietów

Do tego ćwiczenia potrzebne będzie zainstalowanie pakietów SMACH oraz Smach Viewer za pomocą komendy

```
sudo apt-get install ros-melodic-smach ros-melodic-smach-ros ros-melodic-executive-smach ros-melodic-smach-viewer
```

## Przykład 1.
Na potrzeby tego ćwiczenia stworzymy nowy pakiet o nazwie `sipr_lab4`.
Z poziomu `~/sipr_ws/src` 
```
cd ~/sipr_ws/src
```
tworzymy pakiet komendą
```
catkin_create_pkg sipr_lab4 rospy roscpp smach smach_ros
```
uruchamiamy program Visual Studio Code komendą
```
code
```
Należy teraz utworzyć nowy plik `maszyna1.py` w podkatalogu `scripts`.

Można to zrobić klikając prawym przyciskiem myszy na folder `sipr_lab4`(`sipr_ws/src/sipr_lab4`) i klikając `utwórz nowy katalog`,
a następnie klikając na folder `sipr_ws/src/sipr_lab4/scripts` i wybierając  `utwórz nowy plik`.

![](tworzenie_pliku1.png)

Można też otworzyć nowy program poprzez menu główne `File -> New File`.

![](tworzenie_pliku2.png)

Zapisujemy utworzony plik w miejscu docelowym.

Trzeba pamiętać o późniejszym przyznaniu roli "executable" do naszego pliku poprzez
```
cd ~/sipr_ws/src/sipr_lab4/scripts
chmod +x maszyna1.py
```
Inaczej plik nie będzie mógł zostać wykonany.

Po wpisaniu komendy 
``` 
ls
```
nasz plik powinien być zaznaczony na zielono (przed przyznaniem roli executable nazwa pliku wyświetli się na szaro).

Tak samo o przyznaniu tej roli należy pamiętać przy tworzeniu plików z konsoli, np. komendą "touch".

Po utworzeniu pliku musimy zbudować pakiet `sipr_lab4`, robimy to z poziomu `~/sipr_ws` komendą

```
cd ~/sipr_ws
catkin_make
```

Następnie po zbudowaniu

```
source devel/setup.bash
```

Przejdziemy teraz do samego kodu programu

### Tworzenie stanu
```Python
class Foo(smach.State):
   def __init__(self, outcomes=['outcome1', 'outcome2']):
     # Inicjalizacja stanu
   
   def execute(self, userdata):
      # Wykonanie stanu
      if xxxx:
          return 'outcome1'
      else:
          return 'outcome2'
```
  
Aby utworzyć stan, po prostu dziedziczysz z klasy bazowej `State` i implementujesz metodę `State.execute(userdata)`.

W metodzie `init` inicjalizujesz klasę stanu. 

W metodzie wykonania stanu rzeczywista praca jest wykonywana. Tutaj możesz wykonać dowolny kod.

Kiedy stan się kończy, zwraca wynik. Z każdym stanem wiąże się kilka możliwych wyników. Wynikiem jest zdefiniowany przez użytkownika ciąg, który opisuje
zakończenie stanu. Zestaw możliwych wyników mógłby na przykład być `[„powodzenie”, „niepowodzenie”, „niesamowite”]`. Przejście do następnego stanu zostanie 
określone na podstawie wyniku poprzedniego stanu.

### Dodawanie stanu do maszyny stanów
```Py
   1   sm = smach.StateMachine(outcomes=['outcome4','outcome5'])
   2   with sm:
   3      smach.StateMachine.add('FOO', Foo(),
   4                             transitions={'outcome1':'BAR', 
   5                                          'outcome2':'outcome4'})
   6      smach.StateMachine.add('BAR', Bar(),
   7                             transitions={'outcome2':'FOO'})
```
Maszyna stanów, którą otrzymasz wygląda tak:

![](screeny/maszyna1.png)

Czerwone prostokąty pokazują możliwe wyniki kontenera maszyny stanów: `outcome4 i outcome5,` jak określono w linii 1.

W linii 3-5 dodajemy pierwszy stan do kontenera i nazywamy go `FOO`. Konwencja zakłada nazywanie stanów wszystkimi dużymi literami.
Jeśli wynikiem stanu `FOO` jest `outcome1`, to przechodzimy do stanu `BAR`. Jeśli wynikiem stanu `FOO` jest `outcome2`, to cała maszyna stanu zakończy 
działanie z `outcome4`.

Każdy kontener maszyny stanów jest również stanem. Możesz więc zagnieżdżać maszyny stanów, dodając kontener do innego kontenera maszyny stanów.

### Kompletny przykład

```Py
   1 #!/usr/bin/env python
   2 
   3 
   4 import rospy
   5 import smach
   6 import smach_ros
   7 
   8 # definiowanie stanu Foo
   9 class Foo(smach.State):
  10     def __init__(self):
  11         smach.State.__init__(self, outcomes=['outcome1','outcome2'])
  12         self.counter = 0
  13 
  14     def execute(self, userdata):
  15         rospy.loginfo('Executing state FOO')
  16         if self.counter < 3:
  17             self.counter += 1
  18             return 'outcome1'
  19         else:
  20             return 'outcome2'
  21 
  22 
  23 # definiowanie stanu Bar
  24 class Bar(smach.State):
  25     def __init__(self):
  26         smach.State.__init__(self, outcomes=['outcome2'])
  27 
  28     def execute(self, userdata):
  29         rospy.loginfo('Executing state BAR')
  30         return 'outcome2'
  31         
  32 
  33 
  34 
  35 # main
  36 def main():
  37     rospy.init_node('smach_example_state_machine')
  38 
  39     # Tworzenie maszyny stanow SMACH
  40     sm = smach.StateMachine(outcomes=['outcome4', 'outcome5'])
  41 
  42     # Otwarcie kontenera
  43     with sm:
  44         # Dodanie stanow do kontenera
  45         smach.StateMachine.add('FOO', Foo(), 
  46                                transitions={'outcome1':'BAR', 
  47                                             'outcome2':'outcome4'})
  48         smach.StateMachine.add('BAR', Bar(), 
  49                                transitions={'outcome2':'FOO'})
  50 
  51     # Wykonanie planu SMACH
  52     outcome = sm.execute()
  53 
  54 
  55 if __name__ == '__main__':
  56     main()
```
Po przepisaniu i skompilowaniu, ten przykład uruchomimy teraz z poziomu konsoli,

użyjemy komendy

```
rosrun sipr_lab4 maszyna1.py
```

Powyższy przykład powoduje wyświetlenie następujących danych w konsoli:

```
[INFO] 1279835117.234563: Executing state FOO
[INFO] 1279835117.234849: State machine transitioning 'FOO':'outcome1'-->'BAR'
[INFO] 1279835117.235114: Executing state BAR
[INFO] 1279835117.235360: State machine transitioning 'BAR':'outcome2'-->'FOO'
[INFO] 1279835117.235633: Executing state FOO
[INFO] 1279835117.235884: State machine transitioning 'FOO':'outcome1'-->'BAR'
[INFO] 1279835117.236143: Executing state BAR
[INFO] 1279835117.236387: State machine transitioning 'BAR':'outcome2'-->'FOO'
[INFO] 1279835117.236644: Executing state FOO
[INFO] 1279835117.236891: State machine transitioning 'FOO':'outcome1'-->'BAR'
[INFO] 1279835117.237149: Executing state BAR
[INFO] 1279835117.237394: State machine transitioning 'BAR':'outcome2'-->'FOO'
[INFO] 1279835117.237652: Executing state FOO
[INFO] 1279835117.237898: State machine terminating 'FOO':'outcome2':'outcome4'
```

![](screeny/maszyna_2.png)

Widać w jaki sposób maszyna przemieszcza się między stanami i jakie zmienne otrzymujemy na wyjściu każdego ze stanów.

Żeby lepiej zobrazować pracę maszyny użyjemy teraz Smach Viewer.

## Smach Viewer

Smach Viewer to GUI, które pokazuje hierarchię między stanami maszyny. Może wizualizować możliwe przejścia między stanami, a także aktualnie aktywny stan
i wartości danych użytkownika, które są przekazywane między stanami. Smach Viewer wykorzystuje interfejs debugowania SMACH oparty na wiadomościach SMACH 
do zbierania informacji z uruchomionych maszyn stanów.

### Instalacja

W celu zainstalowania pakietu Smach Viewer użyjemy komendy

```
sudo apt-get install smach-viewer
```

### Introspection Server

Kontenery SMACH mogą zapewnić interfejs do debugowania (przez ROS), który pozwala programistom uzyskać pełną introspekcję w maszynie stanu. 
Przeglądarka SMACH może używać tego interfejsu debugowania do wizualizacji i interakcji z maszyną stanu. Aby dodać ten interfejs debugowania 
do automatu stanowego, dodamy następujące wiersze do kodu:

```Py
# Musimy zaimportować smach_ros (jeśli jeszcze tego nie mamy w kodzie)
import smach_ros

# Najpierw opisujesz maszynę stanów
# .....
# Koniec opisywania maszyny stanów

# Utwórz i rozpocznij działanie serwera
sis = smach_ros.IntrospectionServer('server_name', sm, '/SM_ROOT')
sis.start()

# Wykonaj działanie maszyny stanów
outcome = sm.execute()

# Poczekaj na ctrl+c, zatrzymanie maszyny stanów
rospy.spin()
sis.stop()
```

* `server_name`: ta nazwa jest używana do tworzenia przestrzeni dla tematów introspekcji ROS. Możesz nadać temu dowolną nazwę, o ile ta nazwa 
jest unikalna w Twoim systemie. Ta nazwa nie jest wyświetlana w Smach Viewer.

* `SM_ROOT`: twoja maszyna stanów pojawi się pod tą nazwą w Smach Viewer. Możesz więc wybrać dowolną nazwę. Jeśli masz maszyny stanów podrzędne,
które znajdują się w różnych plikach wykonywalnych, możesz je wyświetlić jako hierarchiczne maszyny stanów, wybierając tę nazwę w sprytny sposób:
jeśli maszyna stanów najwyższego poziomu nazywa się `SM_TOP`, możesz wywołać stan podrzędny maszyny `SM_TOP / SM_SUB`, a przeglądarka rozpozna maszynę
podrzędną jako część maszyny stanu najwyższego.

Smach Viewer automatycznie przejdzie przez kontenery potomne `sm`, jeśli takie istnieją. Zatem wystarczy podłączyć
jeden serwer introspekcji do maszyny stanów najwyższego poziomu, a nie do maszyn podrzędnych. Po utworzeniu instancji serwera introspekcji będzie on 
ogłaszał zestaw tematów z nazwami utworzonymi przez dołączenie do nazwy serwera nadanej mu podczas budowy. 
W takim przypadku ogłoszone byłyby trzy tematy:

* `/server_name/smach/container_structure`
* `/server_name/smach/container_status`
* `/server_name/smach/container_init`

Pierwsze dwa publikują informacje i zdarzenia dotyczące struktury i stanu kontenerów SMACH obsługiwanych przez `„server_name”`.
Trzeci jest tematem ustawiania konfiguracji drzewa SMACH przez ROS.

Argument `„SM_ROOT”` jest po prostu używany do wizualizacji i wymuszonego zagnieżdżania różnych serwerów.

### Kod `maszyna1.py` ze Smach Viewer

U nas kompletny kod, po dodaniu elementów potrzebnych do uruchomienia Smach Viewer wygląda następująco

```Py
#!/usr/bin/env python

import rospy
import smach
import smach_ros

# define state Foo
class Foo(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1','outcome2'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state FOO')
        if self.counter < 3:
            self.counter += 1
            return 'outcome1'
        else:
            return 'outcome2'


# define state Bar
class Bar(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome2'])

    def execute(self, userdata):
        rospy.loginfo('Executing state BAR')
        return 'outcome2'
        



# main
def main():
    rospy.init_node('smach_example_state_machine')

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['outcome4', 'outcome5'])

    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('server_name', sm, '/SM_ROOT')
    sis.start()

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('FOO', Foo(), 
                               transitions={'outcome1':'BAR', 
                                            'outcome2':'outcome4'})
        smach.StateMachine.add('BAR', Bar(), 
                               transitions={'outcome2':'FOO'})

    # Execute SMACH plan
    outcome = sm.execute()

    # Wait for ctrl-c to stop the application
    rospy.spin()
    sis.stop()


if __name__ == '__main__':
    main()
```

### Uruchomienie Smach Viewer

Gdy masz już jeden lub więcej serwerów introspekcji działających w systemie ROS, możesz uruchomić Smach Viewer za pomocą komendy

```
rosrun smach_viewer smach_viewer.py
```

### Widok grafu

Widok grafu (główny widok) przedstawia wszystkie stany maszyny połączone przez ich wyniki. Każdy węzeł reprezentuje stan (lub maszynę podrzędną),
a każdy łuk reprezentuje przejście stanu. Gdy wynik stanu jest taki sam, jak wynik maszyny stanów, nie ma rysowanego łuku, aby uprościć widok. 
Kliknięcie `„show implicit”` pokaże wszystkie przejścia między stanami.

### Głębia widoku

Pole `„depth”` (nagórnym pasku) umożliwia rozwijanie lub zwijanie maszyn podrzędnych. Głębokość 0 oznacza, że pokazujesz tylko maszynę stanów najwyższego poziomu, 
głębokość 1 pokazuje pierwszy poziom maszyn podrzędnych itd. Poziom -1 pokazuje wszystkie poziomy.

### Widok drzewa

Możliwe jest również pokazanie maszyny stanów jako drzewa (przełączenie w lewym górnym rogu). Z tego widoku jasno wynika, kiedy jedna maszyna stanów
jest podrzędną maszyną innego stanu.

## Przekazywanie danych między stanami

### Dane wejściowe i wyjściowe

Stan może wymagać danych wejściowych, żeby wykonać zadanie lub może generować dane wyjściowe, które przekaże innym stanom. Takie dane wejściowe i wyjściowe
nazywane są `userdata`. Podczas tworzenia stanu możesz sprecyzować nazwy pól `userdata`.

```Py
 class Foo(smach.State):
     def __init__(self, outcomes=['outcome1', 'outcome2'],
                        input_keys=['foo_input'],
                        output_keys=['foo_output'])

     def execute(self, userdata):
        # Do something with userdata
        if userdata.foo_input == 1:
            return 'outcome1'
        else:
            userdata.foo_output = 3
            return 'outcome2'
```

`input_keys`: stan deklaruje, że oczekuje, że te pola będą istnieć w danych użytkownika. Metodzie `execute` jest udostępniana kopia struktury `userdata`.
Stan może czytać ze wszystkich pól `userdata`, które wylicza na liście `input_keys`, ale nie może zapisywać w żadnym z tych pól.

`output_keys`: wylicza wszystkie wyjścia, które zapewnia stan. Stan może zapisywać do wszystkich pól w strukturze `userdata`, które są wyliczone
na liście `output_keys`.

Jeśli potrzebujesz modyfikowalnego obiektu wejściowego, musisz określić ten sam klucz w obu `input_keys` i `output_keys`. Jeśli nie przekazujesz obiektów 
lub nie musisz wywoływać metod ani ich modyfikować, powinieneś używać unikalnych nazw w `input_keys` i `output_keys`, aby uniknąć nieporozumień 
i potencjalnych błędów.

### Łączenie danych

Dodając stany do maszyny stanów, trzeba także połączyć pola `userdata`, aby umożliwić stanom przekazywanie danych między sobą. Na przykład, jeśli stan 
`FOO` generuje ` foo_output` a `BAR` potrzebuje `bar_input`, możesz dołączyć te dwa porty `userdata` razem, używając ponownego mapowania nazw:

```Py
  sm_top = smach.StateMachine(outcomes=['outcome4','outcome5'],
                          input_keys=['sm_input'],
                          output_keys=['sm_output'])
  with sm_top:
     smach.StateMachine.add('FOO', Foo(),
                            transitions={'outcome1':'BAR',
                                         'outcome2':'outcome4'},
                            remapping={'foo_input':'sm_input',
                                       'foo_output':'sm_data'})
     smach.StateMachine.add('BAR', Bar(),
                            transitions={'outcome2':'FOO'},
                            remapping={'bar_input':'sm_data',
                                       'bar_output1':'sm_output'})
```
Remapping mapuje `in/output_key` stanu do `userdata`. Więc kiedy używasz go na np. `x:y` to:
`x` musi być `input_key` lub `output_key` stanu
`y` automatycznie stanie się częścią `userdata` maszyny stanów

### Przekazywanie danych między stanami

Możemy użyć mechanizmu `remapping`, żeby przekazać dane ze stanu `FOO` do `BAR`. Musimy użyć tej komendy raz przy dodawaniu `FOO` i raz przy dodawaniu `BAR`:

```Py
FOO: remapping={'foo_output':'sm_user_data'}
BAR: remapping={'bar_input':'sm_user_data'}
```

### Przesyłanie danych między maszynami stanów i stanami

Możemy użyć komendy `remapping`, żeby przesłać dane z stanu `BAR` do maszyny, która zawiera stan `BAR`. Jeśli `sm_output` to dane wyjściowe maszyny stanów wtedy:

```Py
BAR: remapping={'bar_output':'sm_output'}
```

Odwrotnie możemy przekazać dane z maszyny stanów do stanu `FOO`. Jeśli `sm_input` to dane wejściowe maszyny, wtedy:

```Py
FOO: remapping={'foo_input':'sm_input'}
```

Pełny przykład:
```Py
#!/usr/bin/env python

import rospy
import smach
import smach_ros

# definiowanie stanu FOO
class Foo(smach.State):
    def __init__(self):
        smach.State.__init__(self, 
                             outcomes=['outcome1','outcome2'],
                             input_keys=['foo_counter_in'],
                             output_keys=['foo_counter_out'])

    def execute(self, userdata):
        rospy.loginfo('Executing state FOO')
        if userdata.foo_counter_in < 3:
            userdata.foo_counter_out = userdata.foo_counter_in + 1
            return 'outcome1'
        else:
            return 'outcome2'


# definiowanie stanu BAR
class Bar(smach.State):
    def __init__(self):
        smach.State.__init__(self, 
                             outcomes=['outcome1'],
                             input_keys=['bar_counter_in'])
        
    def execute(self, userdata):
        rospy.loginfo('Executing state BAR')
        rospy.loginfo('Counter = %f'%userdata.bar_counter_in)        
        return 'outcome1'
        




def main():
    rospy.init_node('smach_example_state_machine')

    # Tworzenie maszyny stanow
    sm = smach.StateMachine(outcomes=['outcome4'])
    sm.userdata.sm_counter = 0

    # Uruchomienie kontenera
    with sm:
        # Dodanie stanow do kontenera
        smach.StateMachine.add('FOO', Foo(), 
                               transitions={'outcome1':'BAR', 
                                            'outcome2':'outcome4'},
                               remapping={'foo_counter_in':'sm_counter', 
                                          'foo_counter_out':'sm_counter'})
        smach.StateMachine.add('BAR', Bar(), 
                               transitions={'outcome1':'FOO'},
                               remapping={'bar_counter_in':'sm_counter'})


    # Wykonanie zadania maszyny stanów
    outcome = sm.execute()


if __name__ == '__main__':
    main()
```

## Prosty stan akcji

### Pusty komunikat zadania celu akcji

Poniżej przykład pustej wiadomości, który wywoła akcję bez wypełniania czegokolwiek w komunikacie zadania.

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    smach.StateMachine.add('TRIGGER_GRIPPER',
                           SimpleActionState('action_server_namespace',
                                             GripperAction),
                           transitions={'succeeded':'APPROACH_PLUG'})
```

### Komunikat z ustalonym celem akcji

Nieco bardziej zaawansowane użycie pozwala określić na stałe cel, który zostanie przesłany do serwera akcji:

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    gripper_goal = Pr2GripperCommandGoal()
    gripper_goal.command.position = 0.07
    gripper_goal.command.max_effort = 99999
    StateMachine.add('TRIGGER_GRIPPER',
                      SimpleActionState('action_server_namespace',
                                        GripperAction,
                                        goal=gripper_goal),
                      transitions={'succeeded':'APPROACH_PLUG'})
```

### Cel akcji pobrany z `userdata`

Czasem pola w `userdata` zawierają wszystkie struktury konieczne do przekazania celu. W powyższym przykładzie celem działania chwytaka są dwa pola: 
`max_effort` i `position`. Jeśli nasze dane użytkownika zawierają pola: `user_data_max` i `user_data_position` to poniższy kod łączy odpowiednie pola:

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    StateMachine.add('TRIGGER_GRIPPER',
                      SimpleActionState('action_server_namespace',
                                        GripperAction,
                                        goal_slots=['max_effort', 
                                                    'position']),
                      transitions={'succeeded':'APPROACH_PLUG'},
                      remapping={'max_effort':'user_data_max',
                                 'position':'user_data_position'})
```

### Ustalenie parametrów celu on-line

Możesz uzyskać powiadomienie, gdy akcja wymaga celu i możesz utworzyć własną wiadomość o celu.

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    def gripper_goal_cb(userdata, goal):
       gripper_goal = GripperGoal()
       gripper_goal.position.x = 2.0
       gripper_goal.max_effort = userdata.gripper_input
       return gripper_goal

    StateMachine.add('TRIGGER_GRIPPER',
                      SimpleActionState('action_server_namespace',
                                        GripperAction,
                                        goal_cb=gripper_goal_cb,
                                        input_keys=['gripper_input'])
                      transitions={'succeeded':'APPROACH_PLUG'},
                      remapping={'gripper_input':'userdata_input'})
```

### Informacja zwrotna z wykonania akcji

Możesz zapisać wynik akcji bezpośrednio w danych użytkownika swojego stanu.

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    StateMachine.add('TRIGGER_GRIPPER',
                      SimpleActionState('action_server_namespace',
                                        GripperAction,
                                        result_slots=['max_effort', 
                                                      'position']),
                      transitions={'succeeded':'APPROACH_PLUG'},
                      remapping={'max_effort':'user_data_max',
                                 'position':'user_data_position'})
```

### Wgląd w informację zwrotną

Jest to bardzo podobne do ustalenia zwrotnego celu. Pozwala odczytać dowolne dane z pól wyników akcji, a nawet zwrócić inny wynik niż domyślny wynik,
np. `„wykonano”`, `„nie wykonano”`, `„przerwano”`.

```Py
sm = StateMachine(['succeeded','aborted','preempted'])
with sm:
    def gripper_result_cb(userdata, status, result):
       if status == GoalStatus.SUCCEEDED:
          userdata.gripper_output = result.num_iterations
          return 'my_outcome'

    StateMachine.add('TRIGGER_GRIPPER',
                      SimpleActionState('action_server_namespace',
                                        GripperAction,
                                        result_cb=gripper_result_cb,
                                        output_keys=['gripper_output'])
                      transitions={'succeeded':'APPROACH_PLUG'},
                      remapping={'gripper_output':'userdata_output'})
```


## Przykład 2. - Analogia robota pracującego w fabryce

Spróbujmy teraz rozpisać maszynę stanów tak aby obsłużyła zadania robota pracującego w fabryce.
Zadania, które maszyna będzie musiała wykonać to:

* Włączenie robota.
* Sprawdzenie dostępnych zadań dla robota.
* Nawigowanie do taśmy produkcyjnej na żądanie.
* Nawigowanie go magazynu po odbiór przedmiotu.
* Znalezienie przedmiotu w magazynie.
* Dostarczenie przedmiotu do punktu docelowego.
* Udanie się do pozycji wyjściowej (home).

Kolejne etapy działania robota są analogiczne do działania maszyny stanów.
Maszyna Stanów: 

*  Robot musi zostać włączony i zainicjowany, potrzebny będzie przycisk stan włączenia robota `POWER_ON`. 
* Kiedy robot jest uruchomiony musimy sprawdzić, czy są jakieś zadania
dla niego zlecone, użyjemy do tego `BUTTON_STATE`. 
* Po otrzymaniu zadania robot musi udać się do taśmy produkcyjnej, do tego zapiszemy `GO_TO_CONVEYOR_BELT`. Robot będzie miał kilka miejsc,
do których będzie się udawał, zapiszemy kilka: `GO_TO_DELIVERY_AREA`, `GO_TO_STOREHOUSE`, `GO_TO_HOME`, przez realizację działań. 
* Przykładowo pracownik będzie zlecał robotowi zadania, które
wymagają zatwierdzenia poprzez stan `ORDER_CONFIRMATION`. 
* Po zatwierdzeniu zadania, trafia ono również do magazynu, gdzie robot musi odebrać odpowiedni przedmiot przez stan `FIND_ORDER`.

Zdefiniujemy teraz poszczególne stany:

* Uruchomienie robota
```Py
class PowerOnRobot(State):
    def __init__(self):
        State.__init__(self, outcomes=['succeeded'])

    def execute(self, userdata):
        rospy.loginfo("Powering ON robot...")
        time.sleep(2)
        return 'succeeded'
```

* Przyjęcie zadania i udanie się do taśmy
```Py
class ButtonState(State):
    def __init__(self, button_state):
        State.__init__(self, outcomes=['succeeded','aborted','preempted'])
        self.button_state=button_state

    def execute(self, userdata):
        if self.button_state == 1:
            return 'succeeded'
        else:
            return 'aborted'
```

* Zatwierdzenie zadania odebrania przedmiotu z magazynu
```Py
class OrderConfirmation(State):
    def __init__(self, user_confirmation):
        State.__init__(self, outcomes=['succeeded','aborted','preempted'])
        self.user_confirmation=user_confirmation

    def execute(self, userdata):
        time.sleep(2)
        if self.user_confirmation == 1:
            time.sleep(2)
            rospy.loginfo("Confirmation order...")
            time.sleep(2)
            rospy.loginfo("Order confirmed...")
            return 'succeeded'
        else:
            return 'preempted'
```

* Znalezienie przedmiotu w magazynie
```Py
class FindProduct(State):
def __init__(self,storehouse_confirmation):
State.__init__(self, outcomes=['succeeded','aborted','preempted'])
self.storehouse_confirmation=storehouse_confirmation

def execute(self, userdata):
sleep(1)
rospy.loginfo ("Find ordered object")
sleep(5)
if self.storehouse_confirmation == 1:
return 'succeeded'
else:
return 'aborted'
```

* Przemieszczenie robota opiszemy za pomocą servera `move_base`, który akceptuje zadania w odpowiednim formacie wiadomości. Piszemy zapytanie klienta o potrzebny stan:
```Py
move_base_state = SimpleActionState('move_base', MoveBaseAction, goal=nav_goal, result_cb=self.move_base_result_cb, exec_timeout=rospy.Duration(5.0), server_wait_timeout=rospy.Duration(10.0))
```

Podajemy następujące zadania:
```Py
self.waypoints.append(Pose(Point(0.0, 0.0, 0.0), quaternions[3]))
self.waypoints.append(Pose(Point(-1.0, -1.5, 0.0), quaternions[0]))
self.waypoints.append(Pose(Point(1.5, 1.0, 0.0), quaternions[1]))
self.waypoints.append(Pose(Point(2.0, -2.0, 0.0), quaternions[1]))
room_locations = (('conveyor_belt', self.waypoints[0]),
('delivery_area', self.waypoints[1]),
('storehouse', self.waypoints[2]),
('home', self.waypoints[3]))
self.room_locations = OrderedDict(room_locations)
nav_states = {}

for room in self.room_locations.iterkeys(): 
    nav_goal = MoveBaseGoal()
    nav_goal.target_pose.header.frame_id = 'map'
    nav_goal.target_pose.pose = self.room_locations[room]
    move_base_state = SimpleActionState('move_base', MoveBaseAction, goal=nav_goal, result_cb=self.move_base_result_cb, 
    exec_timeout=rospy.Duration(5.0),
    server_wait_timeout=rospy.Duration(10.0))
    nav_states[room] = move_base_state
```

Kod obsługi tego robota będzie wyglądał następująco:
```Py
#!/usr/bin/env python
import rospy
import smach
import time
from smach import State, StateMachine
from smach_ros import SimpleActionState, IntrospectionServer
from geometry_msgs.msg import Twist
from math import  pi
import actionlib
from actionlib import GoalStatus
from geometry_msgs.msg import Pose, Point, Quaternion, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from tf.transformations import quaternion_from_euler
from collections import OrderedDict


class PowerOnRobot(State):
    def __init__(self):
        State.__init__(self, outcomes=['succeeded'])

    def execute(self, userdata):
	rospy.loginfo("Powering ON robot...")
	time.sleep(2)
	return 'succeeded'
	

class ButtonState(State):
    def __init__(self, button_state):
        State.__init__(self, outcomes=['succeeded','aborted','preempted'])
        self.button_state=button_state

    def execute(self, userdata):
	if self.button_state == 1:
           return 'succeeded'
	else:
	   return 'aborted'

class OrderConfirmation(State):
    def __init__(self, user_confirmation):
        State.__init__(self, outcomes=['succeeded','aborted','preempted'])
        self.user_confirmation=user_confirmation

    def execute(self, userdata):
	time.sleep(2)
	if self.user_confirmation == 1:
	   time.sleep(2)
	   rospy.loginfo("Confirmation order...")
	   time.sleep(2)
	   rospy.loginfo("Order confirmed...")
           return 'succeeded'
	else:
	   return 'preempted'

class FindProduct(State):
    def __init__(self,storehouse_confirmation):
        State.__init__(self, outcomes=['succeeded','aborted','preempted'])
        self.storehouse_confirmation=storehouse_confirmation

    def execute(self, userdata):
	sleep(1)
	rospy.loginfo ("Please confirm the order")
	sleep(5)
	if self.storehouse_confirmation == 1:
		return 'succeeded'
	else:
		return 'aborted'

class main():
    def __init__(self):

        rospy.init_node('clean_house', anonymous=False)
        rospy.on_shutdown(self.shutdown)        
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
    	rospy.loginfo("Waiting for move_base action server...")
    	self.move_base.wait_for_server(rospy.Duration(15))
    	rospy.loginfo("Connected to move_base action server")
 
   	quaternions = list()
    	euler_angles = (pi/2, pi, 3*pi/2, 0)
    	for angle in euler_angles:
		q_angle = quaternion_from_euler(0, 0, angle, axes='sxyz')
		q = Quaternion(*q_angle)
		quaternions.append(q)
    
    	# Tworzy liste do przechowywania pozycji
    	self.waypoints = list()
    	self.waypoints.append(Pose(Point(0.0, 0.0, 0.0), quaternions[3]))
    	self.waypoints.append(Pose(Point(-1.0, -1.5, 0.0), quaternions[0]))
    	self.waypoints.append(Pose(Point(1.5, 1.0, 0.0), quaternions[1]))
	self.waypoints.append(Pose(Point(2.0, -2.0, 0.0), quaternions[1]))
    	room_locations = (('conveyor_belt', self.waypoints[0]),
	              ('delivery_area', self.waypoints[1]),
	              ('storehouse', self.waypoints[2]),
		      ('home', self.waypoints[3]))
    
    	# Przechowuje kolejnosc odwiedzanych pomieszczen, zeby robot przemieszczal sie w sekwencji
    	self.room_locations = OrderedDict(room_locations)
        nav_states = {}
        
        for room in self.room_locations.iterkeys():         
            nav_goal = MoveBaseGoal()
            nav_goal.target_pose.header.frame_id = 'map'
            nav_goal.target_pose.pose = self.room_locations[room]
            move_base_state = SimpleActionState('move_base', MoveBaseAction, goal=nav_goal, result_cb=self.move_base_result_cb, 
                                                exec_timeout=rospy.Duration(5.0),
                                                server_wait_timeout=rospy.Duration(10.0))
            nav_states[room] = move_base_state


        sm_order_confirmation = StateMachine(outcomes=['succeeded','aborted','preempted'])
        with sm_order_confirmation:
            StateMachine.add('ORDER_CONFIRMATION_PROCESS', OrderConfirmation(1), transitions={'succeeded':'','aborted':'','preempted':''})

        sm_factory = StateMachine(outcomes=['succeeded','aborted','preempted'])

        with sm_factory:            
            StateMachine.add('POWER_ON', PowerOnRobot(), transitions={'succeeded':'BUTTON_STATE'})            
            StateMachine.add('BUTTON_STATE', ButtonState(1), transitions={'succeeded':'GO_TO_CONVEYOR_BELT','aborted':'','preempted':''})
            StateMachine.add('GO_TO_CONVEYOR_BELT', nav_states['conveyor_belt'], transitions={'succeeded':'ORDER_CONFIRMATION','aborted':'ORDER_CONFIRMATION','preempted':'ORDER_CONFIRMATION'})
            StateMachine.add('ORDER_CONFIRMATION', sm_order_confirmation, transitions={'succeeded':'GO_TO_DELIVERY_AREA','aborted':'GO_TO_STOREHOUSE','preempted':'GO_TO_STOREHOUSE'})
	    #StateMachine.add('ORDER_CONFIRMATION', OrderConfirmation(1), transitions={'succeeded':'GO_TO_DELIVERY_AREA','aborted':'GO_TO_STOREHOUSE','preempted':'GO_TO_STOREHOUSE'})
            StateMachine.add('GO_TO_DELIVERY_AREA', nav_states['delivery_area'], transitions={'succeeded':'DELIVER_PRODUCT','aborted':'DELIVER_PRODUCT','preempted':'DELIVER_PRODUCT'})
	    StateMachine.add('GO_TO_STOREHOUSE', nav_states['storehouse'], transitions={'succeeded':'FIND_PRODUCT','aborted':'GO_TO_STOREHOUSE','preempted':'GO_TO_STOREHOUSE'})
            StateMachine.add('FIND_PRODUCT', FindProduct(1), transitions={'succeeded':'GO_TO_DELIVERY_AREA','aborted':'FIND_PRODUCT','preempted':'FIND_PRODUCT'})
	    StateMachine.add('DELIVER_PRODUCT', nav_states['conveyor_belt'], transitions={'succeeded':'GO_TO_HOME','aborted':'GO_TO_HOME','preempted':'GO_TO_HOME'})
	    StateMachine.add('GO_TO_HOME', nav_states['home'], transitions={'succeeded':'BUTTON_STATE','aborted':'GO_TO_HOME','preempted':'GO_TO_HOME'})


        intro_server = IntrospectionServer('factory', sm_factory, '/SM_ROOT')
        intro_server.start()
        
        # Dzialanie maszyny stanow
        sm_outcome = sm_factory.execute()      
        intro_server.stop()
            
    def move_base_result_cb(self, userdata, status, result):
        if status == actionlib.GoalStatus.SUCCEEDED:
            pass
            
    def shutdown(self):
        rospy.loginfo("Stopping the robot...")
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        rospy.loginfo("Factory robot test finished.")
```

Teraz utworzymy nowy plik (według instrukcji z oprzedniego przykładu)w pakiecie `sipr_lab4` o nazwie `factory.py` i tam zapiszemy powyższy kod.

Po skompilowaniu i uruchomieniu, w Smach Viewer wyświetli się poniższy schemat:

![](screeny/factory2.png)

Widok w terminalu będzie następujący (u góry uruchomiony `roscore`, na dole: po lewej - działanie maszyny stanów, po prawej - uruchomiony Smach Viewer):

![](screeny/factory%201.png)

